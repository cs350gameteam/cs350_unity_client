﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public float movementSpeed;
    Vector3 previousLocation;
    Vector3 currentLocation;
    Vector3 cameraVelocity;
    bool initialSet = false;

	protected  bool readInputBelowUILayer;

    Camera camera;

	// Use this for initialization
	public virtual void Start () {
        previousLocation = currentLocation = cameraVelocity = new Vector3(0, 0, 0);
        camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	public virtual void Update () {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                // handle the raycast hitting something
				handleRaycastHit(hit);
                Debug.Log("Raycast hit something");
            }
            else
            {
                Debug.Log("Raycast didn't hit anything");
                
                // move the camera
                if (!initialSet)
                {
                    previousLocation = ray.origin;
                    initialSet = true;
                }
                else
                {
                    currentLocation = ray.origin;
                    cameraVelocity = (currentLocation - previousLocation);
                    cameraVelocity *= movementSpeed * Time.smoothDeltaTime;
                    Debug.Log("cameraVelocity: " + cameraVelocity);
                    Debug.Log("previousLocation: " + previousLocation);
                    Debug.Log("currentLocation: " + currentLocation);
                    this.transform.position -= cameraVelocity;
                    previousLocation = currentLocation;
                }
                
                
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            initialSet = false;
        }
        
    }

	public virtual void handleRaycastHit(RaycastHit hit){

	}
}
