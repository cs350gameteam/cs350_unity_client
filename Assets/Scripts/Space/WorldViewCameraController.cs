﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class WorldViewCameraController : CameraController {
	public Text selectedPlanetText;
	public GameObject selectedPlanetUI;
	public Transform origin;
	public GameObject selection;
	public MoveState moveState;
	public float selectionZoomSpeed;
	public float selectionZoomDistanceOffset;
	public float originOffsetTolerance;

	public float offset;

	// Use this for initialization
	void Start () {
		base.Start ();
		origin.position = this.transform.position;
		selectedPlanetUI.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();

		if (moveState == MoveState.GoTo) {
			moveTowards (selection.transform);
		}
		if (moveState == MoveState.Return) {
			moveTowards (origin);
		}
	}


	public override void handleRaycastHit(RaycastHit hit){
		Debug.Log ("dealing with  override handleRaycastHit");

		selection = hit.transform.gameObject;
		try{
			SpaceObject spaceObject = selection.GetComponent<SpaceObject> ();
			selectedPlanetText.text = spaceObject.name + " - " + spaceObject.ownerStatement;
			moveState = MoveState.GoTo;
			readInputBelowUILayer = false;
		}
		catch (NullReferenceException nre){
			Debug.LogWarning ("NullReferenceExeption was thrown because this object does not have the " +
				"SpaceObject script attached to it.");
		}
	}

	public void moveTowards(Transform target){
		Vector3 destination = target.transform.position - this.transform.forward * selectionZoomDistanceOffset;
		Vector3 direction = (destination - this.transform.position);
		float mag = direction.magnitude;
		Debug.Log ("Magnitude: " + mag + ", Desired Offset: " + selectionZoomDistanceOffset);

		if (mag > 0) {
			direction.Normalize ();
			direction *= (mag / selectionZoomSpeed) * Time.deltaTime;
			offset = mag;
			this.transform.position += direction;
		}
		if ((mag < selectionZoomDistanceOffset) && (moveState == MoveState.GoTo)) {
			selectedPlanetUI.SetActive (true);
		}
		if (mag < originOffsetTolerance && moveState == MoveState.Return) {
			moveState = MoveState.Stay;
			readInputBelowUILayer = true;
		}
	}

	public void StartReturn(){
		moveState = MoveState.Return;
		selectedPlanetUI.SetActive (false);
	}

}

public enum MoveState{
	Stay,
	GoTo,
	Follow,
	Return
}
