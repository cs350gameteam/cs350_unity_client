﻿using UnityEngine;
using System.Collections;

public class SatelliteRevolver : MonoBehaviour {
	public Vector3 rotationFromHorizon;
	public float rotationSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (rotationFromHorizon * rotationSpeed * Time.deltaTime);
	}
}
