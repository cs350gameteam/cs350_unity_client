﻿using UnityEngine;
using System.Collections;

public class SpaceObject : MonoBehaviour {

	public string id;
	public string name;
	public Factions faction;
	public string ownerStatement;

	// Use this for initialization
	protected void Start () {
		switch (faction) {
		case Factions.TeamA:
			ownerStatement = "Controlled by Team A";
			break;
		case Factions.TeamB:
			ownerStatement = "Controlled by Team B";
			break;
		case Factions.TeamC:
			ownerStatement = "Controlled by Team C";
			break;
		case Factions.TeamD:
			ownerStatement = "Controlled by Team D";
			break;
		default:
			ownerStatement = "No Faction Controls This";
			break;
		}
	
	}
	
	// Update is called once per frame
	protected void Update () {
	
	}


}
