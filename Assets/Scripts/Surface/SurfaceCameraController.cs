﻿using UnityEngine;
using System.Collections;
using System;

public class SurfaceCameraController : CameraController {

	// Use this for initialization
	void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();
	}

	public override void handleRaycastHit(RaycastHit hit){
		GameObject obj = hit.transform.gameObject;
		Debug.Log ("using override");
		try{
			Building building = obj.GetComponent<Building>();
			Debug.Log("Building with id " + building.id + " pressed");
		}
		catch(NullReferenceException nre){

		}
	}
}
