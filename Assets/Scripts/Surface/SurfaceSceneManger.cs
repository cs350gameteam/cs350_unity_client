﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SurfaceSceneManger : MonoBehaviour {
	public string surfaceName;

	// Use this for initialization
	void Start () {
		Building[] buildings = FindObjectsOfType<Building> ();
		for (int i = 0; i < buildings.GetLength(0); i++) {
			Building building = buildings[i];
			building.id = surfaceName + "_" + i;
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void returnToSpaceView(){
		Application.LoadLevel (1);
	}
}
