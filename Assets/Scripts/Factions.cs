﻿using UnityEngine;
using System.Collections;

public enum Factions {
	None,
	TeamA,
	TeamB,
	TeamC,
	TeamD
}
